import tools from '../../utils/tools';

Page({
  data: {
    isHide:false
  },

  async onReady() {
    await tools.initThree('#webglcup');
    tools.init3Boss();
    tools.initHelper();
    tools.initOrbitControl();
    tools.createCubeTexture('animate');
    tools.animate();
  },
 
  onShow() {
    if(this.data.isHide){
      this.setData({
        isHide: false
      });
      tools.reStartAnimate();
    }
  },

  onHide() {
    this.setData({
      isHide: true
    });
    tools.cancelAnimate();
  },

  onUnload: function () {
    tools.cancelAnimate();
    tools.clear();
  },

  touchStart(e) {
    tools.touchStart(e);
  },
  touchMove(e) {
    tools.touchMove(e);
  },
  touchEnd(e) {
    tools.touchEnd(e);
  }
})