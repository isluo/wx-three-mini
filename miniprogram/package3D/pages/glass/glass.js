import tools from '../../utils/tools';

Page({
  data: {
    isHide:false,
    info: []
  },

  async onReady() {
    tools.setInfoCallback(this.setInfo);
    await tools.initThree('#webglcup');
    tools.init3Boss();
    tools.initHelper();
    tools.initOrbitControl();
    tools.createCubeTexture('glass', 2);
    console.log('开始执行animate');
    tools.animate();
  },
 
  onShow() {
    if(this.data.isHide){
      this.setData({
        isHide: false
      });
      tools.reStartAnimate();
    }
  },

  onHide() {
    this.setData({
      isHide: true
    });
    tools.cancelAnimate();
  },

  onUnload: function () {
    tools.cancelAnimate();
    tools.clear();
  },

  touchStart(e) {
    tools.touchStart(e);
  },
  touchMove(e) {
    tools.touchMove(e);
  },
  touchEnd(e) {
    tools.touchEnd(e);
  },
  setInfo(str){
    this.data.info.push(str);
    this.setData({
      info: this.data.info
    })
  }
})