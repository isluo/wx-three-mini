import tools from '../../utils/tools';

Page({
  data: {
    isHide:false
  },

  async onReady() {
    await tools.initThree('#webgl');
    tools.init3Boss();
    tools.initHelper();
    tools.createCubeTexture('box');
    // tools.initBox();

    console.log('开始渲染');
    tools.animate();

  },
 
  onShow() {
    if(this.data.isHide){
      this.setData({
        isHide: false
      });
      tools.reStartAnimate();
    }
  },

  onHide() {
    this.setData({
      isHide: true
    });
    tools.cancelAnimate();
  },

  onUnload: function () {
    tools.cancelAnimate();
    tools.clearBox();
    tools.clear();
  },
})